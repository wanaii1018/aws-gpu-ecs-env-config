#!/bin/bash
echo NVCC version check:
nvcc --version

echo PATH check:
echo "$PATH"

echo CUDA_HOME check:
echo "$CUDA_HOME"

echo CD check:
echo "$PWD"

echo CREATE VENV PATH check:
py_dir="$PWD/seminal-venv"
echo "$py_dir"

apt update
apt upgrade -y
apt install python3.10-venv -y

rm -rf $py_dir # remove old venv if existed
python3 -m venv $py_dir # create new clean venv

py="$py_dir/bin/python3"
py_pip="$py -m pip"
pip_install="$py_pip install"


$pip_install --upgrade pip setuptools
$pip_install torch==2.1.2 torchvision==0.16.2 torchaudio==2.1.2 --index-url https://download.pytorch.org/whl/cu121 # install corresponding torch
$py -c 'import torch; print(torch.cuda.is_available())'

git clone 'https://github.com/IDEA-Research/Grounded-Segment-Anything.git'

export AM_I_DOCKER=False
export BUILD_WITH_CUDA=True
# export CUDA_HOME="/usr/local/cuda-12.4" # need to be removed in docker env

cd ./Grounded-Segment-Anything
echo "# INSTALL: Segment-Anything"
$pip_install -e segment_anything

echo "# INSTALL: GroundingDINO"
#$pip_install --no-build-isolation -e GroundingDINO
cd ./GroundingDINO
$pip_install -r ./requirements.txt
$py ./setup.py install
cd ..

echo "# INSTALL: diffusers[torch]"
$pip_install --upgrade diffusers[torch]


echo "# INSTALL: grounded-sam-osx"
git submodule update --init --recursive
$pip_install openmim
$py -m mim install mmcv-full
$pip_install -r ./grounded-sam-osx/requirements.txt
cd ./grounded-sam-osx/transformer_utils
$py ./setup.py install
cd ../..


echo "# INSTALL: recognize-anything"
git clone https://github.com/xinyu1205/recognize-anything.git
$pip_install -r ./recognize-anything/requirements.txt
$pip_install --upgrade pip setuptools
$pip_install -e ./recognize-anything


echo "# INSTALL OTHER DEPS"
$pip_install opencv-python pycocotools matplotlib onnxruntime onnx ipykernel
$pip_install flask flask_cors waitress mysql-connector-python pika faiss-cpu pymongo opensearch-py apify-client Cython boto3 scikit-image typing_extensions

wget https://github.com/IDEA-Research/GroundingDINO/releases/download/v0.1.0-alpha/groundingdino_swint_ogc.pth
wget https://dl.fbaipublicfiles.com/segment_anything/sam_vit_h_4b8939.pth
wget https://huggingface.co/spaces/xinyu1205/Tag2Text/resolve/main/ram_swin_large_14m.pth
wget https://huggingface.co/spaces/xinyu1205/Tag2Text/resolve/main/tag2text_swin_14m.pth

$py ./grounding_dino_demo.py

cd ..
python3 ./app.py