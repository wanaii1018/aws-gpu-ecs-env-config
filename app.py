from flask import Flask
from flask_cors import CORS, cross_origin
from waitress import serve
import socket

hostname = socket.gethostname()
ip_address = socket.gethostbyname(hostname)

print(f"Hostname: {hostname}")
print(f"IP Address: {ip_address}")

app = Flask(__name__)
cors = CORS(app)


@app.route('/')
@cross_origin()
def hello():
    return 'HELLO'


if __name__ == '__main__':
    print('SERVER STARTED: 8888')
    serve(
        app,
        host='0.0.0.0',
        port=8888,
        threads=500,
    )
